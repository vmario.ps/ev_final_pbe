# Generated by Django 5.0 on 2023-07-20 23:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Greengo', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='evento',
            name='c_lugar',
        ),
        migrations.RemoveField(
            model_name='evento',
            name='c_tevento',
        ),
        migrations.DeleteModel(
            name='Lugar',
        ),
        migrations.DeleteModel(
            name='Tipo_evento',
        ),
    ]
