from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Evento (models.Model):
    codigo = models.AutoField(primary_key=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, null=False)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_evento = models.DateField(default= '2001-09-11')
    descripcion = models.CharField(max_length=252, null=False)
    def __str__(self):
        txt = "{0}: {1} ({2}), {3}"
        return txt.format(self.codigo, self.nombre, self.fecha_evento, self.descripcion)